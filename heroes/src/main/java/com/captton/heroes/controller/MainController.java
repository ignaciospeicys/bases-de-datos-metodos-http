package com.captton.heroes.controller;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.heroes.model.Heroe;
import com.captton.heroes.repository.HeroeDao;

@RestController
@RequestMapping({"/heroes"})
public class MainController {
	
	@Autowired //automaticamente instancia la variable en tiempo de ejecucion
	HeroeDao heroedao;
	
	@PostMapping
	public Heroe create(@RequestBody Heroe heroe) {
		
		return heroedao.save(heroe);
		
	}
	
	@GetMapping
	public ResponseEntity<Object> findAll() {
		
		List<Heroe> heroesFound = heroedao.findAll();
		
		if(heroesFound.size() > 0) {
			
			for(Heroe her: heroesFound) {
				
				System.out.println("Nombre: "+her.getNombre());
			}
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 0);
			obj.put("results", heroesFound);
			
			return ResponseEntity.ok().body(obj.toString());
			
		} else {
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 1);
			obj.put("results", "no se encontraron heroes");
			
			
			return ResponseEntity.ok().body(obj.toString());
			
		}
		
	}
	
	@GetMapping(path= {"/{id}"})
	public ResponseEntity<Object> findById(@PathVariable long id) {
		
		JSONObject obj = new JSONObject();
		Heroe heroeFound = heroedao.findById(id).orElse(null);
		
		if(heroeFound != null) {
			
			JSONObject objheroe = new JSONObject();
			
			objheroe.put("nombre", heroeFound.getNombre());
			objheroe.put("img", heroeFound.getImg());
			objheroe.put("aparicion", heroeFound.getAparicion());
			objheroe.put("casa", heroeFound.getCasa());
			objheroe.put("bio", heroeFound.getBio());
			
			System.out.println("Encontro: "+heroeFound.getNombre());
			
			obj.put("error", 0);
			obj.put("results", objheroe);
			
			return ResponseEntity.ok().body(obj.toString());
			
		} else {
		
			
			obj.put("error", 1);
			obj.put("results", "no se encontro el registro");
			
			return ResponseEntity.ok().body(obj.toString());
			
		}
		
	}
	
	@DeleteMapping(path = {"/{id}"})
	public ResponseEntity<Object> delete(@PathVariable("id") long id) {
		
		heroedao.deleteById(id);
		
		return ResponseEntity.ok().body("Registro borrado");
				
	}
	
	@PutMapping(value ="/{id}")
	public ResponseEntity<Heroe> update(@PathVariable("id") long id, @RequestBody Heroe heroe){
		
		return heroedao.findById(id).map(record -> {
			record.setNombre(heroe.getNombre());
			record.setBio(heroe.getBio());
			record.setImg(heroe.getImg());
			record.setAparicion(heroe.getAparicion());
			record.setCasa(heroe.getCasa());
			
			Heroe updated = heroedao.save(record);
			
			return ResponseEntity.ok().body(updated);
			
		}).orElse(ResponseEntity.notFound().build());
	}
	
	@GetMapping(path= {"/buscar/{nom}"})
	public ResponseEntity<Object> findByNombre(@PathVariable("nom") String nombre) {
		
		List<Heroe> heroesFound = heroedao.findByNombre(nombre);
		
		if(heroesFound.size() > 0) {
			
			for(Heroe her: heroesFound) {
				
				System.out.println("Nombre: "+her.getNombre());
			}
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 0);
			obj.put("results", heroesFound);
			
			return ResponseEntity.ok().body(obj.toString());
			
		} else {
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 1);
			obj.put("results", "no se encontraron heroes");
			
			
			return ResponseEntity.ok().body(obj.toString());
			
		}
		
	}
	
	
	

}
